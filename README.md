AWS_Cognito for Unity
====

AWS Cognitoを使ったサーバーレス、ユーザー管理機能

## Description

* SignUp - ユーザー登録
  * Create User - Cognitoにユーザー登録
  * EMail Confirm - メールアドレスの認証コードでの確認
    * Resend Code - 認証コードの再送信
* SignIn - ユーザーとしてログインする機能
  * Login - ログイン（手動 or 起動時自動ログイン）
  * Change Detail - ユーザー情報の編集
    * Profile - 説明文
    * NickName - ニックネーム
    * EMail - メールアドレス
    * Password - パスワード
    * WebSite - ウェブサイト
    * etc...
  * Forgot Password - パスワードを忘れた場合の処理

## Licence

The MIT License (MIT)
Copyright (c) 2019 ConsoleSoup

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Author

[ConsoleSoup](https://twitter.com/consolesoup)

[うさぎのとおりみち](https://twitter.com/rabitroad)