using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_InputText : MonoBehaviour
{
    [SerializeField] private InputField _inputField;
    private UnityEngine.Events.UnityAction<string> _inputCallback;
    private UnityEngine.Events.UnityAction _closeCallback;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(string text, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<string> inputCallback)
    {
        _inputField.text = text;
        _closeCallback = closeCallback;
        _inputCallback = inputCallback;
    }

    public void CloseOnClick()
    {
        _closeCallback();
        Destroy(this.gameObject);
    }

    public void InputOnClick()
    {
        _inputCallback(_inputField.text);
        Destroy(this.gameObject);
    }
}
