using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_Dropdown : MonoBehaviour
{
    [SerializeField] private Dropdown _dropDown;
    private UnityEngine.Events.UnityAction<int> _selectCallback;
    private UnityEngine.Events.UnityAction _closeCallback;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(string[] options, int value, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        List<Dropdown.OptionData> _options = new List<Dropdown.OptionData>();
        for (int i = 0; i < options.Length; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = options[i];
            _options.Add(option);
        }
        _dropDown.options = _options;

        _dropDown.value = value;
        _closeCallback = closeCallback;
        _selectCallback = selectCallback;
    }

    public void CloseOnClick()
    {
        _closeCallback();
        Destroy(this.gameObject);
    }

    public void SelectOnClick()
    {
        _selectCallback(_dropDown.value);
        Destroy(this.gameObject);
    }
}
