using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_Calendar : MonoBehaviour
{
    [SerializeField] private Dropdown _yearDropdown;
    [SerializeField] private Dropdown _monthDropdown;
    [SerializeField] private RectTransform _dayList;
    [SerializeField] private Font _font;
    private UnityEngine.Events.UnityAction<DateTime> _selectCallback;
    private UnityEngine.Events.UnityAction _closeCallback;
    private DateTime _initializeDate;
    private int year;
    private int month;
    private int day;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(DateTime date, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<DateTime> selectCallback)
    {
        _closeCallback = closeCallback;
        _selectCallback = selectCallback;
        year = date.Year;
        month = date.Month;
        day = date.Day;
        _initializeDate = DateTime.Now;

        List<Dropdown.OptionData> _yearOptions = new List<Dropdown.OptionData>();
        int yearValue = 0;
        for (int i = 0; i < 2; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = (_initializeDate.Year+i)+"�N";
            _yearOptions.Add(option);
            if ((_initializeDate.Year+i) == year) yearValue = i;
        }
        _yearDropdown.options = _yearOptions;
        _yearDropdown.value = yearValue;
        _yearDropdown.onValueChanged.AddListener((int value) => {
            _reload();
        });

        List<Dropdown.OptionData> _monthOptions = new List<Dropdown.OptionData>();
        int monthValue = 0;
        for (int i = 0; i < 12; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = i+1 + "��";
            _monthOptions.Add(option);
            if (i+1 == month) monthValue = i;
        }
        _monthDropdown.options = _monthOptions;
        _monthDropdown.value = monthValue;
        _monthDropdown.onValueChanged.AddListener((int value) => {
            _reload();
        });

        _reload();
    }

    private void _reload()
    {
        for (int i = 0; i < _dayList.childCount; i++) Destroy(_dayList.GetChild(i).gameObject);

        string startDateStr = (_yearDropdown.value+_initializeDate.Year).ToString().PadLeft(4, '0') + "-" + (_monthDropdown.value+1).ToString().PadLeft(2, '0') + "-01";
        DateTime startDate = DateTime.ParseExact(startDateStr, "yyyy-MM-dd", null);
        startDate = startDate.AddDays(-(startDate.DayOfWeek == DayOfWeek.Sunday ? 6 : ((int)startDate.DayOfWeek) - 1));

        for (int y = 0; y < 5; y++)
        {
            for (int x = 0; x < 7; x++)
            {
                GameObject buttonGameObject = new GameObject(startDate.ToString("yyyy-MM-dd"));
                buttonGameObject.transform.SetParent(_dayList);

                Image image = buttonGameObject.AddComponent<Image>();
                if (startDate.Year == year && startDate.Month == month && startDate.Day == day) image.color = Color.gray;
                else image.color = Color.white;
                image.raycastTarget = true;
                if (startDate.Month == _monthDropdown.value + 1)
                {
                    DateTime date = startDate;

                    Button button = buttonGameObject.AddComponent<Button>();
                    button.image = image;
                    button.onClick.AddListener(() => {
                        year = date.Year;
                        month = date.Month;
                        day = date.Day;
                        _reload();
                    });
                }

                buttonGameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(_dayList.sizeDelta.x/7, _dayList.sizeDelta.y / 5);
                buttonGameObject.transform.localPosition = new Vector3(x*_dayList.sizeDelta.x/7, -y*_dayList.sizeDelta.y/5)+new Vector3(((_dayList.sizeDelta.x/7)-_dayList.sizeDelta.x)/2, (_dayList.sizeDelta.y-(_dayList.sizeDelta.y/5))/2);

                GameObject textGameObject = new GameObject("Text");
                textGameObject.transform.SetParent(buttonGameObject.transform);

                Text textUI = textGameObject.AddComponent<Text>();
                textUI.font = _font;
                textUI.text = startDate.ToString("dd");
                textUI.resizeTextForBestFit = false;
                textUI.fontSize = 60;
                textUI.alignment = TextAnchor.MiddleCenter;
                if (startDate.Year == year && startDate.Month == month && startDate.Day == day) textUI.color = Color.white;
                else if (startDate.Month != _monthDropdown.value+1) textUI.color = new Color(1/4f*3, 1/4f*3, 1/4f*3);
                else if (startDate.DayOfWeek == DayOfWeek.Saturday) textUI.color = new Color(1/2f, 1/2f, 1f);
                else if (startDate.DayOfWeek == DayOfWeek.Sunday) textUI.color = new Color(1f, 1/2f, 1/2f);
                else textUI.color = new Color(1/2f, 1/2f, 1/2f);

                textGameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(_dayList.sizeDelta.x / 7 * 0.8f, _dayList.sizeDelta.y / 5 * 0.8f);
                textGameObject.transform.localPosition = Vector3.zero;

                startDate = startDate.AddDays(1);
            }
        }
    }

    public void CloseOnClick()
    {
        _closeCallback();
        Destroy(this.gameObject);
    }

    public void SelectOnClick()
    {
        DateTime date = DateTime.ParseExact(year.ToString().PadLeft(4,'0')+"-"+month.ToString().PadLeft(2,'0')+"-"+day.ToString().PadLeft(2,'0'), "yyyy-MM-dd", null);
        _selectCallback(date);
        Destroy(this.gameObject);
    }
}
