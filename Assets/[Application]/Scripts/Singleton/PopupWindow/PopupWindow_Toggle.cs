using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_Toggle : MonoBehaviour
{
    [SerializeField] private RectTransform _backGround;
    [SerializeField] private RectTransform _toggleRoot;
    [SerializeField] private Button _togglePrefab;
    private List<bool> _toggleList = new List<bool>();
    private UnityEngine.Events.UnityAction<bool[]> _selectCallback;
    private UnityEngine.Events.UnityAction _closeCallback;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(string[] options, bool[] toggles, int row, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<bool[]> selectCallback)
    {
        if (row <= 0) row = 1;

        for (int i = 0; i < _toggleRoot.childCount; i++) Destroy(_toggleRoot.GetChild(i).gameObject);

        _toggleList.Clear();
        _toggleList.AddRange(toggles);

        float width = _toggleRoot.sizeDelta.x/row;
        while (width < 100 && row > 1)
        {
            row--;
            width = _toggleRoot.sizeDelta.x/row;
        }
        float height = Mathf.Max(100, width);

        float _toggleRootHeight = height*_toggleList.Count/row;
        Vector2 sizeDelta = new Vector2(_backGround.sizeDelta.x, _backGround.sizeDelta.y-_toggleRoot.sizeDelta.y+_toggleRootHeight);
        _backGround.sizeDelta = sizeDelta;
        _toggleRoot.sizeDelta = new Vector2(_toggleRoot.sizeDelta.x, _toggleRootHeight);

        for (int i = 0; i < _toggleList.Count; i++)
        {
            Button button = Instantiate<Button>(_togglePrefab, _toggleRoot.transform);
            button.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
            button.transform.localPosition = new Vector3(-_toggleRoot.sizeDelta.x/2+(i%row*width), _toggleRoot.sizeDelta.y/2-(i/row*height));
            button.image.color = _toggleList[i] ? Color.white : Color.gray;
            int index = i;
            button.onClick.AddListener(() => {
                _toggleList[index] = !_toggleList[index];
                button.image.color = _toggleList[index] ? Color.white : Color.gray;
            });
            Transform textTransform = button.transform.Find("Text");
            Text text = textTransform.GetComponent<Text>();
            text.text = i < options.Length ? options[i] : "";
        }
        
        _closeCallback = closeCallback;
        _selectCallback = selectCallback;
    }

    public void CloseOnClick()
    {
        _closeCallback();
        Destroy(this.gameObject);
    }

    public void SelectOnClick()
    {
        _selectCallback(_toggleList.ToArray());
        Destroy(this.gameObject);
    }
}
