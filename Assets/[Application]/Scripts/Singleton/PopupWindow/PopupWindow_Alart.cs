using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_Alart : MonoBehaviour
{
    [SerializeField] private Font _font;
    [SerializeField] private Sprite _buttonBack;
    [SerializeField] private RectTransform _backGroundRect;
    [SerializeField] private Button _closeButton;
    [SerializeField] private float _space = 40;
    [SerializeField] private Text _title;
    [SerializeField] private Text _detail;
    [SerializeField] private RectTransform _buttonRoot;
    private UnityEngine.Events.UnityAction<int> _selectCallback;
    private UnityEngine.Events.UnityAction _closeCallback;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(string detail, UnityEngine.Events.UnityAction closeCallback)
    {
        Initialize(null, detail, null, closeCallback, null);
    }
    public void Initialize(string title, string detail, UnityEngine.Events.UnityAction closeCallback)
    {
        Initialize(title, detail, null, closeCallback, null);
    }
    public void Initialize(string detail, string[] options, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        Initialize(null, detail, options, closeCallback, selectCallback);
    }
    public void Initialize(string title, string detail, string[] options, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        float height = 0;
        float widthMax = _backGroundRect.sizeDelta.x-(_space*2);

        _closeButton.gameObject.SetActive(closeCallback != null);

        RectTransform titleRect = _title.GetComponent<RectTransform>();
        if (string.IsNullOrEmpty(title))
        {
            _title.text = null;
            titleRect.sizeDelta = Vector2.zero;
        }
        else
        {
            _title.text = title;
            titleRect.sizeDelta = new Vector2(widthMax, _title.preferredHeight);
            height += _space+_title.preferredHeight;
        }

        RectTransform detailRect = _detail.GetComponent<RectTransform>();
        if (string.IsNullOrEmpty(detail))
        {
            _detail.text = null;
            detailRect.sizeDelta = Vector2.zero;
        }
        else
        {
            _detail.text = detail;
            detailRect.sizeDelta = new Vector2(widthMax, _detail.preferredHeight);
            height += _space+_detail.preferredHeight;
        }

        for (int i = 0; i < _buttonRoot.childCount; i++) Destroy(_buttonRoot.GetChild(i).gameObject);
        if (options == null || options.Length == 0 || selectCallback == null)
        {
            _buttonRoot.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        }
        else
        {
            _buttonRoot.sizeDelta = new Vector2(widthMax, 0);

            Vector2[] buttonSizes = new Vector2[options.Length];
            for (int i = 0; i < options.Length; i++)
            {
                GameObject textObject = new GameObject();
                Text textComponent = textObject.AddComponent<Text>();
                RectTransform textRect = textComponent.GetComponent<RectTransform>();
                textRect.sizeDelta = new Vector2(_buttonRoot.sizeDelta.x, _backGroundRect.sizeDelta.y);
                textComponent.font = _font;
                textComponent.fontSize = 80;
                textComponent.text = options[i];
                textRect.sizeDelta = new Vector2(_buttonRoot.sizeDelta.x, textComponent.preferredHeight);
                buttonSizes[i] = new Vector2(textComponent.preferredWidth+_space, textComponent.preferredHeight+_space);
                Destroy(textObject);
            }

            float _height = 0;
            Vector2 size = Vector2.zero;
            int count = 0;
            for (int i = 0; i < buttonSizes.Length; i++)
            {
                count++;
                Vector2 button = buttonSizes[i];
                if (size.x + button.x + _space > _buttonRoot.sizeDelta.x)
                {
                    _height += size.y + (_height == 0 ? 0 : _space);
                    for (int j = i - (count - 1); j < i; j++)
                    {
                        buttonSizes[j].x = (_buttonRoot.sizeDelta.x-((count-1) == 0 ? 0 : (count-2)*_space)) *(buttonSizes[j].x/size.x);
                        buttonSizes[j].y = size.y;
                    }
                    size = button;
                    count = 1;
                }
                else
                {
                    size.x += button.x;
                    size.y = Mathf.Max(size.y, button.y);
                }
            }
            if (count != 0)
            {
                _height += size.y + (_height == 0 ? 0 : _space);
                for (int j = buttonSizes.Length-count; j < buttonSizes.Length; j++)
                {
                    buttonSizes[j].x = (_buttonRoot.sizeDelta.x-(count == 0 ? 0 : (count-1)*_space)) * (buttonSizes[j].x / size.x);
                    buttonSizes[j].y = size.y;
                }
            }

            _buttonRoot.sizeDelta = new Vector2(widthMax, _height);
            height += _space+_height;

            Vector2 space = Vector2.zero;
            for (int i = 0; i < buttonSizes.Length; i++)
            {
                GameObject buttonObject = new GameObject("Button" + (i + 1));
                buttonObject.transform.SetParent(_buttonRoot);

                Image imageComponent = buttonObject.AddComponent<Image>();
                imageComponent.sprite = _buttonBack;
                imageComponent.type = Image.Type.Sliced;
                imageComponent.fillCenter = true;

                Outline outline = buttonObject.AddComponent<Outline>();
                outline.effectColor = new Color(1/6f*5, 1/6f*5, 1/6f*5);
                outline.effectDistance = new Vector2(4, 4);

                Button buttonComponent = buttonObject.AddComponent<Button>();
                int index = i;
                buttonComponent.onClick.AddListener(() => {
                    SelectOnClick(index);
                });
                buttonComponent.image = imageComponent;


                buttonObject.GetComponent<RectTransform>().sizeDelta = buttonSizes[i];
                if (space.x+buttonSizes[i].x > _buttonRoot.sizeDelta.x)
                {
                    space.x = 0;
                    space.y -= buttonSizes[i].y+_space;
                }
                float x = space.x == 0 ? 0 : space.x+_space;
                float y = space.y;
                buttonObject.transform.localPosition = new Vector3(x-(_buttonRoot.sizeDelta.x-buttonSizes[i].x)/2, y+(_buttonRoot.sizeDelta.y-buttonSizes[i].y)/2);
                space.x = x+buttonSizes[i].x;

                GameObject textObject = new GameObject("Text");
                textObject.transform.SetParent(buttonObject.transform);
                Text textComponent = textObject.AddComponent<Text>();
                textObject.GetComponent<RectTransform>().sizeDelta = buttonSizes[i];
                textObject.transform.localPosition = Vector3.zero;

                textComponent.font = _font;
                textComponent.fontSize = 80;
                textComponent.alignment = TextAnchor.MiddleCenter;
                textComponent.color = new Color(1/3f, 1/3f, 1/3f);
                textComponent.text = options[i];
            }
        }

        _backGroundRect.sizeDelta = new Vector2(_backGroundRect.sizeDelta.x, height+_space);
        _backGroundRect.localPosition = Vector2.zero;

        titleRect.localPosition = new Vector3(0, (_backGroundRect.sizeDelta.y-titleRect.sizeDelta.y)/2-(titleRect.sizeDelta.y == 0 ? 0 : _space));
        _detail.transform.localPosition = new Vector3(0, titleRect.localPosition.y-(titleRect.sizeDelta.y+detailRect.sizeDelta.y)/2-(detailRect.sizeDelta.y == 0 ? 0 : _space));
        _buttonRoot.localPosition = new Vector3(0, detailRect.localPosition.y-(detailRect.sizeDelta.y+_buttonRoot.sizeDelta.y)/2-(_buttonRoot.sizeDelta.y == 0 ? 0 : _space));

        _closeCallback = closeCallback;
        _selectCallback = selectCallback;
    }

    public void CloseOnClick()
    {
        if (_closeCallback != null) _closeCallback();
        Destroy(this.gameObject);
    }

    private void SelectOnClick(int value)
    {
        if (_selectCallback != null) _selectCallback(value);
        Destroy(this.gameObject);
    }
}
