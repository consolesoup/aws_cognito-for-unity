using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class PopupWindowManager : Singleton<PopupWindowManager>
{
    private string _thisScene = "PopupWindow";

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator _sceneLoadCheck()
    {
        Scene scene = SceneManager.GetSceneByName(_thisScene);
        if (scene == null || !scene.isLoaded) yield return SceneManager.LoadSceneAsync(_thisScene, LoadSceneMode.Additive);


    }

    public void ShowInputField(string text, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<string> inputCallback)
    {
        PopupWindow_InputText inputText = Resources.Load<PopupWindow_InputText>(typeof(PopupWindow_InputText).Name);
        inputText = Instantiate<PopupWindow_InputText>(inputText);
        inputText.Initialize(text, closeCallback, inputCallback);
    }

    public void ShowTimeSelect(TimeSpan span, bool hour, bool minute, bool second, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<TimeSpan> selectCallback)
    {
        PopupWindow_TimeSelect timeSelect = Resources.Load<PopupWindow_TimeSelect>(typeof(PopupWindow_TimeSelect).Name);
        timeSelect = Instantiate<PopupWindow_TimeSelect>(timeSelect);
        timeSelect.Initialize(span, hour, minute, second, closeCallback, selectCallback);
    }

    public void ShowDropdown(string[] options, int value, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        PopupWindow_Dropdown dropdown = Resources.Load<PopupWindow_Dropdown>(typeof(PopupWindow_Dropdown).Name);
        dropdown = Instantiate<PopupWindow_Dropdown>(dropdown);
        dropdown.Initialize(options, value, closeCallback, selectCallback);
    }

    public void ShowToggle(string[] options, bool[] toggles, int row, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<bool[]> selectCallback)
    {
        PopupWindow_Toggle toggle = Resources.Load<PopupWindow_Toggle>(typeof(PopupWindow_Toggle).Name);
        toggle = Instantiate<PopupWindow_Toggle>(toggle);
        toggle.Initialize(options, toggles, row, closeCallback, selectCallback);
    }

    public void ShowCalendar(DateTime date, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<DateTime> selectCallback)
    {
        PopupWindow_Calendar calendar = Resources.Load<PopupWindow_Calendar>(typeof(PopupWindow_Calendar).Name);
        calendar = Instantiate<PopupWindow_Calendar>(calendar);
        calendar.Initialize(date, closeCallback, selectCallback);
    }

    public void ShowAlart(string detail, UnityEngine.Events.UnityAction closeCallback)
    {
        ShowAlart(null, detail, null, closeCallback, null);
    }
    public void ShowAlart(string title, string detail, UnityEngine.Events.UnityAction closeCallback)
    {
        ShowAlart(title, detail, null, closeCallback, null);
    }
    public void ShowAlart(string detail, string[] options, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        ShowAlart(null, detail, options, closeCallback, selectCallback);
    }
    public void ShowAlart(string title, string detail, string[] options, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<int> selectCallback)
    {
        PopupWindow_Alart alart = Resources.Load<PopupWindow_Alart>(typeof(PopupWindow_Alart).Name);
        alart = Instantiate<PopupWindow_Alart>(alart);
        alart.Initialize(title, detail, options, closeCallback, selectCallback);
    }
}
