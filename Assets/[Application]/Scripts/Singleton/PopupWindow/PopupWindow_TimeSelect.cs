using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWindow_TimeSelect : MonoBehaviour
{
    [SerializeField] private Dropdown _hourDropDown;
    [SerializeField] private Dropdown _minuteDropDown;
    [SerializeField] private Dropdown _secondDropDown;
    private UnityEngine.Events.UnityAction<TimeSpan> _selectCallback;
    private UnityEngine.Events.UnityAction _closeCallback;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Initialize(TimeSpan span, bool hour, bool minute, bool second, UnityEngine.Events.UnityAction closeCallback, UnityEngine.Events.UnityAction<TimeSpan> selectCallback)
    {
        if (closeCallback == null) return;
        if (!hour && !minute && !second) closeCallback();
        if (selectCallback == null || span == null) return;

        _closeCallback = closeCallback;
        _selectCallback = selectCallback;

        List<Dropdown.OptionData> hourOptions = new List<Dropdown.OptionData>();
        for (int i = 0; i < 24; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = i.ToString().PadLeft(2, '0');
            hourOptions.Add(option);
        }
        _hourDropDown.options = hourOptions;
        _hourDropDown.value = span.Hours;
        _hourDropDown.interactable = hour;

        List<Dropdown.OptionData> minuteOptions = new List<Dropdown.OptionData>();
        for (int i = 0; i < 60; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = i.ToString().PadLeft(2, '0');
            minuteOptions.Add(option);
        }
        _minuteDropDown.options = minuteOptions;
        _minuteDropDown.value = span.Minutes;
        _minuteDropDown.interactable = minute;

        List<Dropdown.OptionData> secondOptions = new List<Dropdown.OptionData>();
        for (int i = 0; i < 60; i++)
        {
            Dropdown.OptionData option = new Dropdown.OptionData();
            option.text = i.ToString().PadLeft(2, '0');
            secondOptions.Add(option);
        }
        _secondDropDown.options = secondOptions;
        _secondDropDown.value = span.Seconds;
        _secondDropDown.interactable = second;
    }

    public void CloseOnClick()
    {
        _closeCallback();
        Destroy(this.gameObject);
    }

    public void SelectOnClick()
    {
        _selectCallback(new TimeSpan(_hourDropDown.value, _minuteDropDown.value, _secondDropDown.value));
        Destroy(this.gameObject);
    }
}
