using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    protected static T _instance;
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                T[] createds = GameObject.FindObjectsOfType<T>();
                if (createds.Length > 0)
                {
                    _instance = createds[0];
                    for (int i = 1; i < createds.Length; i++) Destroy(createds[i].gameObject);
                }

                if (_instance == null)
                {
                    GameObject gameObject = new GameObject(typeof(T).Name);
                    _instance = gameObject.AddComponent<T>();
                }
            }

            if (_instance != null) DontDestroyOnLoad(_instance);

            return _instance;
        }
    }
}
