using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SignIn()
    {
        string message = CognitoManager.Instance.SignIn();
        if (string.IsNullOrEmpty(message))
        {
            message = CognitoManager.Instance.GetUserData();
            if (string.IsNullOrEmpty(message)) SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.World);
            else SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn);
        }
        else SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn);
    }
}