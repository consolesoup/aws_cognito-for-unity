using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class AccountMenuBehaviour : MonoBehaviour
{
    [SerializeField] private Text _nameAndID, _level, _profile, _webSite, _birthday, _id_edit, _level_edit;
    [SerializeField] private InputField _nameInput, _profileInput, _websiteInput, _birthdayInput;
    [SerializeField] private GameObject _viewRoot, _editRoot;

    [Space]

    [SerializeField] private InputField _oldPassword;
    [SerializeField] private InputField _newPassword, _typoCheckPassword;
    [SerializeField] private Text _oldPasswordLog, _newPasswordLog, _typoCheckPasswordLog;

    // Start is called before the first frame update
    void Start()
    {
        _oldPassword.onValueChanged.AddListener((string value) => {
            _oldPasswordLog.text = _passwordFormatCheck(value);
        });
        _newPassword.onValueChanged.AddListener((string value) => {
            _newPasswordLog.text = _passwordFormatCheck(value);
            if (string.IsNullOrEmpty(_newPasswordLog.text) && _oldPassword.text == _newPassword.text) _newPasswordLog.text = "Old Passwardと同じ内容になっています。";
            if (string.IsNullOrEmpty(_typoCheckPasswordLog.text) && _newPassword.text != _typoCheckPassword.text) _typoCheckPasswordLog.text = "New Passwordと同じ内容を入力してください。";
        });
        _typoCheckPassword.onValueChanged.AddListener((string value) => {
            _typoCheckPasswordLog.text = _passwordFormatCheck(value);
            if (string.IsNullOrEmpty(_typoCheckPasswordLog.text) && _newPassword.text != _typoCheckPassword.text) _typoCheckPasswordLog.text = "New Passwordと同じ内容を入力してください。";
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (_nameAndID != null) _nameAndID.text = CognitoManager.Instance.NickName+" ("+CognitoManager.Instance.ID+")";
        if (_level != null) _level.text = "Lv."+CognitoManager.Instance.Level;
        if (_profile != null) _profile.text = CognitoManager.Instance.Profile;
        if (_webSite != null) _webSite.text = CognitoManager.Instance.WebSite;
        if (_birthday != null) _birthday.text = "Birthday: "+(string.IsNullOrEmpty(CognitoManager.Instance.Birthday) ? "未設定" : DateTime.ParseExact(CognitoManager.Instance.Birthday, "yyyy-MM-dd", null).ToString("yyyy/MM/dd"));

        if (_id_edit != null) _id_edit.text = "(" + CognitoManager.Instance.ID + ")";
        if (_level_edit != null) _level_edit.text = "Lv." + CognitoManager.Instance.Level;
    }

    public void ProfileEdit()
    {
        _nameInput.text = CognitoManager.Instance.NickName;
        _profileInput.text = CognitoManager.Instance.Profile;
        _websiteInput.text = CognitoManager.Instance.WebSite;
        _birthdayInput.text = string.IsNullOrEmpty(CognitoManager.Instance.Birthday) ? null : DateTime.ParseExact(CognitoManager.Instance.Birthday, "yyyy-MM-dd", null).ToString("yyyyMMdd");

        _viewRoot.SetActive(false);
        _editRoot.SetActive(true);
    }

    public void SaveProfile()
    {
        if (string.IsNullOrEmpty(_nameInput.text))
        {
            PopupWindowManager.Instance.ShowAlart("Profile Save", "Nick Nameが入力されていません。", () => { });
            return;
        }

        if (string.IsNullOrEmpty(_profileInput.text))
        {
            PopupWindowManager.Instance.ShowAlart("Profile Save", "Profileが入力されていません。", () => { });
            return;
        }

        if (!string.IsNullOrEmpty(_websiteInput.text) && !_websiteInput.text.Contains("https://") && !_websiteInput.text.Contains("http://"))
        {
            PopupWindowManager.Instance.ShowAlart("Profile Save", "WebSiteは'https://'か'https://'ではじまる必要があります。", () => { });
            return;
        }
        
        if (!string.IsNullOrEmpty(_birthdayInput.text) && DateTime.ParseExact(_birthdayInput.text, "yyyyMMdd", null) == null)
        {
            PopupWindowManager.Instance.ShowAlart("Profile Save", "Birthdayのフォーマットは'yyyyMMdd'である必要があります。", () => { });
            return;
        }

        List<CognitoManager.PlayerPrefsKey> keyList = new List<CognitoManager.PlayerPrefsKey>();
        List<string> valueList = new List<string>();

        if (_nameInput.text != CognitoManager.Instance.NickName)
        {
            keyList.Add(CognitoManager.PlayerPrefsKey.NickName);
            valueList.Add(_nameInput.text);
        }

        if (_profileInput.text != CognitoManager.Instance.Profile)
        {
            keyList.Add(CognitoManager.PlayerPrefsKey.Profile);
            valueList.Add(_profileInput.text);
        }

        if (_websiteInput.text != CognitoManager.Instance.WebSite)
        {
            keyList.Add(CognitoManager.PlayerPrefsKey.WebSite);
            valueList.Add(_websiteInput.text);
        }

        if (_birthdayInput.text != (string.IsNullOrEmpty(CognitoManager.Instance.Birthday) ? null : DateTime.ParseExact(CognitoManager.Instance.Birthday, "yyyyMMdd", null).ToString("yyyy/MM/dd")))
        {
            keyList.Add(CognitoManager.PlayerPrefsKey.Birthday);
            valueList.Add(DateTime.ParseExact(_birthdayInput.text, "yyyyMMdd", null).ToString("yyyy-MM-dd"));
        }

        if (keyList.Count > 0 && valueList.Count > 0)
        {
            string message = CognitoManager.Instance.SetUserData(keyList.ToArray(), valueList.ToArray());
            if (string.IsNullOrEmpty(message))
            {
                _viewRoot.SetActive(true);
                _editRoot.SetActive(false);
            }
            else if (message == CognitoManager.Instance.AccessTokenHasExpired || message == CognitoManager.Instance.AccessTokenHasBeenRevoked || message == CognitoManager.Instance.UserIsNotConfirmed)
            {
                PopupWindowManager.Instance.ShowAlart("Profile Save", "ProfileのSaveに失敗しました。\n" + message+"\nSignIn画面に戻ります。", () => {
                    SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single);
                });
            }
            else PopupWindowManager.Instance.ShowAlart("Profile Save", "ProfileのSaveに失敗しました。\n"+message, () => { });
        }
        else
        {
            _viewRoot.SetActive(true);
            _editRoot.SetActive(false);
        }
    }

    private string _passwordFormatCheck(string value)
    {
        //Password
        string passwordLog = null;
        if (string.IsNullOrEmpty(value)) passwordLog = "未入力です。";
        else if (value.Length < 6) passwordLog = "6文字以上にしてください。";
        else if (Regex.IsMatch(value, "[^a-zA-Z0-9]+")) passwordLog = "アルファベットと数字である必要があります。";
        else if (!Regex.IsMatch(value, "[a-z]")) passwordLog = "最低1文字のアルファベットを含む必要があります。";
        else if (!Regex.IsMatch(value, "[0-9]")) passwordLog = "最低1文字の数字を含む必要があります。";
        return passwordLog;
    }

    public void ChangePassword()
    {
        _oldPasswordLog.text = _passwordFormatCheck(_oldPassword.text);
        _newPasswordLog.text = _passwordFormatCheck(_newPassword.text);
        if (string.IsNullOrEmpty(_newPasswordLog.text) && _oldPassword.text == _newPassword.text) _newPasswordLog.text = "Old Passwardと同じ内容になっています。";
        _typoCheckPasswordLog.text = _passwordFormatCheck(_typoCheckPassword.text);
        if (string.IsNullOrEmpty(_typoCheckPasswordLog.text) && _newPassword.text != _typoCheckPassword.text) _typoCheckPasswordLog.text = "New Passwordと同じ内容を入力してください。";

        if (!string.IsNullOrEmpty(_oldPasswordLog.text) || !string.IsNullOrEmpty(_newPasswordLog.text) || !string.IsNullOrEmpty(_typoCheckPasswordLog.text)) return;

        string message = CognitoManager.Instance.ChangePassword(_oldPassword.text, _newPassword.text);
        if (string.IsNullOrEmpty(message)) PopupWindowManager.Instance.ShowAlart("パスワードの変更", "パスワードを変更しました。", () => { });
        else if (message == CognitoManager.Instance.AccessTokenHasExpired || message == CognitoManager.Instance.AccessTokenHasBeenRevoked || message == CognitoManager.Instance.UserIsNotConfirmed)
        {
            PopupWindowManager.Instance.ShowAlart("パスワードの変更", "パスワードの変更に失敗しました。\n" + message + "\nSignIn画面に戻ります。", () => {
                SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single);
            });
        }
        else PopupWindowManager.Instance.ShowAlart("パスワードの変更", "パスワードの変更に失敗しました。\n"+message, () => { });
    }
}
