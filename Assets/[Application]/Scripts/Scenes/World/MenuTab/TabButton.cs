using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabButton : MonoBehaviour
{
    [SerializeField] private int _layer;
    public int Layer { get { return _layer; } }

    [SerializeField] private bool _defaultActive;
    public bool DefaultActive { get { return _defaultActive; } }

    [SerializeField] private Button _button;
    [SerializeField] private Transform _layout;

    private UnityEngine.Events.UnityAction<int, int> selectAction;
    public UnityEngine.Events.UnityAction<int, int> SelectAction { set { selectAction = value; } }

    // Start is called before the first frame update
    void Start()
    {
        _button.interactable = !DefaultActive;
        _layout.gameObject.SetActive(DefaultActive);

        _button.onClick.AddListener(() => {
            if (selectAction != null) selectAction(_layer, this.gameObject.GetInstanceID());
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TabSelect(int instanceID)
    {
        if (this.gameObject.GetInstanceID() == instanceID)
        {
            _button.interactable = false;
            _layout.gameObject.SetActive(true);
        }
        else
        {
            _button.interactable = true;
            _layout.gameObject.SetActive(false);
        }
    }
}
