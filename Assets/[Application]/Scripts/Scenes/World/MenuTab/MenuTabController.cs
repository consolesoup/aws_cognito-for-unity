using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTabController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        TabButton[] tabButtons = GameObject.FindObjectsOfType<TabButton>();
        foreach (TabButton tabButton in tabButtons) tabButton.SelectAction = (int layer, int instaceID) => {
            foreach (TabButton tab in tabButtons)
            {
                if (tab.Layer != layer) continue;
                tab.TabSelect(instaceID);
            }
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
