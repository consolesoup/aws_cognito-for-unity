using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMenuBehaviour : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SignOutOnClicked()
    {
        string message = CognitoManager.Instance.SignOut();
        if (string.IsNullOrEmpty(message)) SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single);
        else if (message == CognitoManager.Instance.AccessTokenHasExpired || message == CognitoManager.Instance.AccessTokenHasBeenRevoked || message == CognitoManager.Instance.UserIsNotConfirmed) SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single);
        else PopupWindowManager.Instance.ShowAlart("SignOut失敗", message, () => { });
    }

    public void DeleteOnClicked()
    {
        PopupWindowManager.Instance.ShowAlart("アカウントの削除", "アカウントの削除を行いますか？\n※アカウントを削除すると関連するデータも消えてしまいます。", new string[]{ "Delete", "Cancel" }, null, (int value) => {
            if (value != 0) return;

            string message = CognitoManager.Instance.DeleteUser();
            if (string.IsNullOrEmpty(message)) SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single);
            else if (message == CognitoManager.Instance.AccessTokenHasBeenRevoked) PopupWindowManager.Instance.ShowAlart("アカウントの削除に失敗", message, () => { SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.SignIn, UnityEngine.SceneManagement.LoadSceneMode.Single); });
            else PopupWindowManager.Instance.ShowAlart("アカウントの削除に失敗", message, () => { });
        });
    }
}
