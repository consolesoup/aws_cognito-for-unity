using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;

public class CognitoSign : MonoBehaviour
{
    [SerializeField] private InputField _idField, _passwordField, _nameField, _mailField, _accessCodeField;
    [SerializeField] private Text _connectionLog, _idLog, _passwordLog, _nameLog, _mailLog, _accessCodeLog;
    [SerializeField] private Button _signupButton, _confirmButton, _signInButton;

    void Start()
    {
        _connectionLog.text = null;
        _idLog.text = null;
        _passwordLog.text = null;
        _nameLog.text = null;
        _mailLog.text = null;
        _accessCodeLog.text = null;

        _idField.onValueChanged.AddListener(_idChanged);
        _passwordField.onValueChanged.AddListener(_passwordChanged);
        _nameField.onValueChanged.AddListener(_nameChanged);
        _mailField.onValueChanged.AddListener(_mailChanged);
        _accessCodeField.onValueChanged.AddListener(_accessCodeChanged);

        _idChanged(CognitoManager.Instance.ID);
        _passwordChanged(CognitoManager.Instance.Password);
        _nameChanged(CognitoManager.Instance.NickName);
        _mailChanged(CognitoManager.Instance.Mail);

        _signInButton.onClick.Invoke();
    }

    void Update()
    {

    }

    private void _idChanged(string text)
    {
        if (_idField.text != text) _idField.text = text;
        if (CognitoManager.Instance.ID != text) CognitoManager.Instance.ID = text;

        _checkInput(2);
    }

    private void _passwordChanged(string text)
    {
        if (_passwordField.text != text) _passwordField.text = text;
        if (CognitoManager.Instance.Password != text) CognitoManager.Instance.Password = text;

        _checkInput(2);
    }

    private void _nameChanged(string text)
    {
        if (_nameField.text != text) _nameField.text = text;
        if (CognitoManager.Instance.NickName != text) CognitoManager.Instance.NickName = text;

        _checkInput(0);
    }

    private void _mailChanged(string text)
    {
        if (_mailField.text != text) _mailField.text = text;
        if (CognitoManager.Instance.Mail != text) CognitoManager.Instance.Mail = text;

        _checkInput(0);
    }

    private void _accessCodeChanged(string text)
    {
        if (_accessCodeField.text == text) _accessCodeField.text = text;
        if (CognitoManager.Instance.AccessCode != text) CognitoManager.Instance.AccessCode = text;

        _checkInput(1);
    }

    public void CheckInput(int type)
    {
        _checkInput(type);
    }
    private bool _checkInput(int type)
    {
        bool error = false;
        
        //ID
        string idLog = null;
        if (string.IsNullOrEmpty(CognitoManager.Instance.ID))
        {
            error = true;
            idLog = "未入力です。";
        }
        else if (CognitoManager.Instance.ID.Length <= 3 || 20 < CognitoManager.Instance.ID.Length)
        {
            error = true;
            idLog = "4文字以上、20文字以内にしてください。";
        }
        _idLog.text = idLog;

        //Password
        string passwordLog = null;
        if (string.IsNullOrEmpty(CognitoManager.Instance.Password))
        {
            error = true;
            passwordLog = "未入力です。";
        }
        else if (CognitoManager.Instance.Password.Length < 6)
        {
            error = true;
            passwordLog = "6文字以上にしてください。";
        }
        else if (Regex.IsMatch(CognitoManager.Instance.Password, "[^a-zA-Z0-9]+"))
        {
            error = true;
            passwordLog = "アルファベットと数字である必要があります。";
        }
        else if (!Regex.IsMatch(CognitoManager.Instance.Password, "[a-z]"))
        {
            error = true;
            passwordLog = "最低1文字のアルファベットを含む必要があります。";
        }
        else if (!Regex.IsMatch(CognitoManager.Instance.Password, "[0-9]"))
        {
            error = true;
            passwordLog = "最低1文字の数字を含む必要があります。";
        }
        _passwordLog.text = passwordLog;
        
        if (type == 0)
        {
            //Name
            string nameLog = null;
            if (string.IsNullOrEmpty(CognitoManager.Instance.NickName))
            {
                error = true;
                nameLog = "未入力です。";
            }
            _nameLog.text = nameLog;

            //Mail
            string mailLog = null;
            if (string.IsNullOrEmpty(CognitoManager.Instance.Mail))
            {
                error = true;
                mailLog = "未入力です。";
            }
            else if (!CognitoManager.Instance.IsValidEmail(CognitoManager.Instance.Mail))
            {
                error = true;
                mailLog = "不正なメールアドレスです。";
            }
            _mailLog.text = mailLog;
        }

        if (type == 1)
        {
            string accessCode = null;

            if (string.IsNullOrEmpty(CognitoManager.Instance.AccessCode))
            {
                error = true;
                accessCode = "未入力です。";
            }

            _accessCodeLog.text = accessCode;
        }

        _connectionLog.text = null;

        return error;
    }

    public void SignIn()
    {
        if (!_checkInput(2))
        {
            string message = CognitoManager.Instance.SignIn();
            if (string.IsNullOrEmpty(message))
            {
                message = CognitoManager.Instance.GetUserData();
                if (string.IsNullOrEmpty(message)) SceneLoadManager.Instance.LoadScene(SceneLoadManager.SceneType.World);
                else _connectionLog.text = "[SignIn Success] " + message;
            }
            else if (message == CognitoManager.Instance.UserDoesNotExist) PopupWindowManager.Instance.ShowAlart("SignIn失敗", message, () => { _signupButton.onClick.Invoke(); });
            else if (message == CognitoManager.Instance.UserIsNotConfirmed) PopupWindowManager.Instance.ShowAlart("SignIn失敗", message, () => { _confirmButton.onClick.Invoke(); });
            else  _connectionLog.text = message;
        }
    }

    public void SignUp()
    {
        if (!_checkInput(0))
        {
            string message = CognitoManager.Instance.SignUp();

            if (string.IsNullOrEmpty(message)) PopupWindowManager.Instance.ShowAlart("SignUp", "SignUpに成功しました。", () => { });

            if (message == CognitoManager.Instance.UserIsNotConfirmed)
            {
                PopupWindowManager.Instance.ShowAlart("SignUp", "SignUpに成功しました。\n登録したMailにAccess Codeを送信しました。\nConfirmからMailの認証を行ってください。", () => { _confirmButton.onClick.Invoke(); });
                _connectionLog.text = null;
            }
            else _connectionLog.text = message;
        }
    }

    public void ResendCode()
    {
        bool error = false;

        string idLog = null;
        if (string.IsNullOrEmpty(CognitoManager.Instance.ID))
        {
            error = true;
            idLog = "未入力です。";
        }
        else if (CognitoManager.Instance.ID.Length <= 3 || 20 < CognitoManager.Instance.ID.Length)
        {
            error = true;
            idLog = "4文字以上、20文字以内にしてください。";
        }
        _idLog.text = idLog;
        
        if (!error)
        {
            string message = CognitoManager.Instance.ReSendCode();
            _connectionLog.text = message;
        }
    }

    public void Confirm()
    {
        if (!_checkInput(1))
        {
            string message = CognitoManager.Instance.Confirm();
            if (string.IsNullOrEmpty(message)) PopupWindowManager.Instance.ShowAlart("Access Code Confirm", "Access Codeの認証に成功しました。\nSignInが可能になりました。", () => { _signInButton.onClick.Invoke(); });
            _connectionLog.text = message;
        }
    }
}