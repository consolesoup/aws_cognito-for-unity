using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoadManager : Singleton<SceneLoadManager>
{
    [SerializeField] private Image _image;

    [SerializeField][SceneName] private string SplashScreen;
    [SerializeField][SceneName] private string SignIn;
    [SerializeField][SceneName] private string World;

    public enum SceneType
    {
        SplashScreen,
        SignIn,
        World
    }

    private bool _loadAnimation = false;

    [RuntimeInitializeOnLoadMethod()]
    static void InitializeOnLoad()
    {
        SceneLoadManager.Instance.Start();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_image == null)
        {
            SceneLoadManager _sceneLoadManager = Resources.Load<SceneLoadManager>(typeof(SceneLoadManager).ToString());
            SceneLoadManager instance = Instantiate<SceneLoadManager>(_sceneLoadManager);
            instance.name = _sceneLoadManager.name;
            DontDestroyOnLoad(instance);
            _instance = instance;

            Destroy(this.gameObject);
        }
        else
        {
            _image.color = Color.black;
            StartCoroutine(_disableImage());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadScene(SceneType type)
    {
        LoadScene(type, LoadSceneMode.Single);
    }
    public void LoadScene(SceneType type, LoadSceneMode mode)
    {
        StartCoroutine(_loadScene(type, mode));
    }
    private IEnumerator _loadScene(SceneType type, LoadSceneMode mode)
    {
        yield return new WaitUntil(() => { return !_loadAnimation; });
        _loadAnimation = true;

        _image.enabled = true;
        Color activeColor = Color.black;
        Color beforeColor = _image.color;
        float count = 0;
        while (_image.color != activeColor)
        {
            _image.color = Color.Lerp(beforeColor, activeColor, count);
            yield return new WaitForEndOfFrame();
            count += Time.deltaTime;
        }

        string scene = SignIn;
        switch (type)
        {
            case SceneType.SplashScreen:
                scene = SplashScreen;
                break;
            case SceneType.SignIn:
                scene = SignIn;
                break;
            case SceneType.World:
                scene = World;
                break;
        }

        yield return SceneManager.LoadSceneAsync(scene, mode);

        StartCoroutine(_disableImage());
    }

    private IEnumerator _disableImage()
    {
        Color disableColor = Color.black*Color.clear;
        Color beforeColor = _image.color;
        float count = 0;
        while (_image.color != disableColor)
        {
            _image.color = Color.Lerp(beforeColor, disableColor, count);
            yield return new WaitForEndOfFrame();
            count += Time.deltaTime;
        }
        _image.enabled = false;
        _loadAnimation = false;
    }
}
