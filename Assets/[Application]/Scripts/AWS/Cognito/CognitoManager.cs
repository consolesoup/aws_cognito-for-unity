using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using Amazon;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;

public class CognitoManager : Singleton<CognitoManager>
{
    private AmazonCognitoIdentityProviderClient _client = new AmazonCognitoIdentityProviderClient(new Amazon.Runtime.AnonymousAWSCredentials(), RegionEndpoint.USWest2);
    private string _clientId = "AWS Congnito Client ID";
    private string _userPoolId = "User Pool ID";

    public enum PlayerPrefsKey
    {
        ID,
        Password,
        NickName,
        Mail,
        MailVerified,

        Token,

        Level,
        Profile,
        WebSite,
        Birthday
    }

    private string _accessCode;
    public string ID { get { return PlayerPrefs.GetString(PlayerPrefsKey.ID.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.ID.ToString(), value); } }
    public string Password { get { return PlayerPrefs.GetString(PlayerPrefsKey.Password.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.Password.ToString(), value); } }
    public string NickName { get { return PlayerPrefs.GetString(PlayerPrefsKey.NickName.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.NickName.ToString(), value); } }
    public string Mail { get { return PlayerPrefs.GetString(PlayerPrefsKey.Mail.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.Mail.ToString(), value); } }
    public bool MailVerified { get { return PlayerPrefs.GetInt(PlayerPrefsKey.MailVerified.ToString()) == 0 ? false : true; } set { PlayerPrefs.SetInt(PlayerPrefsKey.MailVerified.ToString(), value ? 1 : 0); } }
    public string AccessCode { get { return _accessCode; } set { _accessCode = value; } }

    public int Level { get { return Math.Max(PlayerPrefs.GetInt(PlayerPrefsKey.Level.ToString()), 1); } set { PlayerPrefs.SetInt(PlayerPrefsKey.Level.ToString(), value); } }
    public string Profile { get { return PlayerPrefs.GetString(PlayerPrefsKey.Profile.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.Profile.ToString(), value); } }
    public string WebSite { get { return PlayerPrefs.GetString(PlayerPrefsKey.WebSite.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.WebSite.ToString(), value); } }
    public string Birthday { get { return PlayerPrefs.GetString(PlayerPrefsKey.Birthday.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.Birthday.ToString(), value); } }

    private string _token { get { return PlayerPrefs.GetString(PlayerPrefsKey.Token.ToString()); } set { PlayerPrefs.SetString(PlayerPrefsKey.Token.ToString(), value); } }

    private string _attributeNameFromKey(PlayerPrefsKey key)
    {
        string name = string.Empty;
        switch (key)
        {
            case PlayerPrefsKey.Birthday:
                name = "birthdate";
                break;
            case PlayerPrefsKey.Level: //カスタムの属性で追加した"level"
                name = "custom:level";
                break;
            case PlayerPrefsKey.Mail:
                name = "email";
                break;
            case PlayerPrefsKey.MailVerified:
                name = "email_verified";
                break;
            case PlayerPrefsKey.NickName:
                name = "nickname";
                break;
            case PlayerPrefsKey.Profile:
                name = "profile";
                break;
            case PlayerPrefsKey.WebSite:
                name = "website";
                break;
        }
        return name;
    }

    public readonly string IncorrectUsernameOrPassword = "IDまたはパスワードが間違っています。";
    public readonly string UserIsNotConfirmed = "ConfirmにてMailのアクセスコードを認証してください。";
    public readonly string AccessTokenHasExpired = "ログイン情報の有効期限が過ぎています。\nSignInしなおしてください。";
    public readonly string AccessTokenHasBeenRevoked = "ログイン情報が取り消されています。\nSignInしなおしてください。";
    public readonly string UserDoesNotExist = "アカウントが見つかりませんでした。\nSignUpにてアカウントを作成してください。";

    void Start()
    {

    }

    void Update()
    {

    }

    public bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email)) return false;

        try
        {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                  RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match)
            {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();

                // Pull out and process domain name (throws ArgumentException on invalid)
                var domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException e)
        {
            return false;
        }
        catch (ArgumentException e)
        {
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

    public string SignIn()
    {
        string message = null;

        try
        {
            InitiateAuthRequest authRequest = new InitiateAuthRequest();
            authRequest.AuthFlow = AuthFlowType.USER_PASSWORD_AUTH;
            authRequest.AuthParameters.Add("USERNAME", ID);
            authRequest.AuthParameters.Add("PASSWORD", Password);
            authRequest.ClientId = _clientId;

            InitiateAuthResponse authResponse = _client.InitiateAuth(authRequest);
            if (authResponse.HttpStatusCode == System.Net.HttpStatusCode.OK) _token = authResponse.AuthenticationResult.AccessToken;
            else message = "SignInに失敗しました。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Incorrect username or password.") message = IncorrectUsernameOrPassword;
            else if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (UserNotFoundException unfe)
        {
            if (unfe.Message == "User does not exist.") message = UserDoesNotExist;
            else message = unfe.Message;
        }
        catch (UserNotConfirmedException unce)
        {
            if (unce.Message == "User is not confirmed.") message = UserIsNotConfirmed;
            else message = unce.Message;
        }
        catch (AmazonCognitoIdentityProviderException acipe)
        {
            message = acipe.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string SignUp()
    {
        string message = null;

        try
        {
            SignUpRequest signUpRequest = new SignUpRequest
            {
                ClientId = _clientId,
                Password = Password,
                Username = ID,
            };

            AttributeType emailAttribute = new AttributeType
            {
                Name = _attributeNameFromKey(PlayerPrefsKey.Mail),
                Value = Mail
            };
            signUpRequest.UserAttributes.Add(emailAttribute);

            AttributeType nicknameAttribute = new AttributeType
            {
                Name = _attributeNameFromKey(PlayerPrefsKey.NickName),
                Value = NickName
            };
            signUpRequest.UserAttributes.Add(nicknameAttribute);

            AttributeType levelAttribute = new AttributeType
            {
                Name = _attributeNameFromKey(PlayerPrefsKey.Level),
                Value = 1.ToString()
            };
            signUpRequest.UserAttributes.Add(levelAttribute);

            AttributeType profileAttribute = new AttributeType
            {
                Name = _attributeNameFromKey(PlayerPrefsKey.Profile),
                Value = "Welcome to RabitRoad Account."
            };
            signUpRequest.UserAttributes.Add(profileAttribute);

            // リザルトオブジェクト取得
            SignUpResponse signUpResponse = _client.SignUp(signUpRequest);

            if (signUpResponse.HttpStatusCode != System.Net.HttpStatusCode.OK) message = "SignUpに失敗しました。";
            else if (!signUpResponse.UserConfirmed) message = UserIsNotConfirmed;
        }
        catch (UsernameExistsException uee)
        {
            message = "そのIDは既に存在します。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string SignOut()
    {
        string message = null;

        try
        {
            GlobalSignOutRequest signOutRequest = new GlobalSignOutRequest();
            signOutRequest.AccessToken = _token;

            GlobalSignOutResponse signOutResponse = _client.GlobalSignOut(signOutRequest);

            if (signOutResponse.HttpStatusCode != System.Net.HttpStatusCode.OK) message = "SignOutに失敗しました。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (UserNotConfirmedException unce)
        {
            if (unce.Message == "User is not confirmed.") message = UserIsNotConfirmed;
            else message = unce.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string DeleteUser()
    {
        string message = null;

        try
        {
            DeleteUserRequest deleteUserRequest = new DeleteUserRequest();
            deleteUserRequest.AccessToken = _token;

            DeleteUserResponse deleteUserResponse = _client.DeleteUser(deleteUserRequest);

            if (deleteUserResponse.HttpStatusCode != System.Net.HttpStatusCode.OK) message = "アカウントの削除に失敗しました。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string ReSendCode()
    {
        string message = null;

        try
        {
            ResendConfirmationCodeRequest codeRequest = new ResendConfirmationCodeRequest();
            codeRequest.Username = ID;
            codeRequest.ClientId = _clientId;

            ResendConfirmationCodeResponse codeResponse = _client.ResendConfirmationCode(codeRequest);
            if (codeResponse.HttpStatusCode == System.Net.HttpStatusCode.OK) PopupWindowManager.Instance.ShowAlart("Access Code ReSend", codeResponse.CodeDeliveryDetails.Destination + "にAccess Codeを再送信しました。", () => { });
            else message = "Access Codeの再送信に失敗しました。";
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string Confirm()
    {
        string message = null;

            try
            {
                ConfirmSignUpRequest confirmSignUpRequest = new ConfirmSignUpRequest();
                confirmSignUpRequest.Username = ID;
                confirmSignUpRequest.ConfirmationCode = AccessCode;
                confirmSignUpRequest.ClientId = _clientId;

                ConfirmSignUpResponse confirmSignUpResult = _client.ConfirmSignUp(confirmSignUpRequest);
                if (confirmSignUpResult.HttpStatusCode != System.Net.HttpStatusCode.OK) message = "Access Codeの認証に失敗しました。";
            }
            catch (NotAuthorizedException nae)
            {
                if (nae.Message == "Incorrect username or password.") message = IncorrectUsernameOrPassword;
                else if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
                else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
                else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
                else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
                else message = nae.Message;
            }
            catch (CodeMismatchException cme)
            {
                if (cme.Message == "Invalid verification code provided, please try again.") message = "無効な確認コードが入力されました。もう一度お試しください。";
                else message = cme.Message;
            }
            catch (Exception e)
            {
                message = e.Message;
            }

        return message;
    }

    public string GetUserData()
    {
        string message = null;

        try
        {
            // AccessTokenを元にUser名を取得
            GetUserRequest userRequest = new GetUserRequest();
            userRequest.AccessToken = _token;

            GetUserResponse userResponse = _client.GetUser(userRequest);
            if (userResponse.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                ID = userResponse.Username;
                foreach (AttributeType attribute in userResponse.UserAttributes)
                {
                    PlayerPrefsKey[] keys = (PlayerPrefsKey[])Enum.GetValues(typeof(PlayerPrefsKey));
                    foreach (PlayerPrefsKey key in keys)
                    {
                        if (attribute.Name == _attributeNameFromKey(key))
                        {
                            switch (key)
                            {
                                case PlayerPrefsKey.Birthday:
                                    Birthday = attribute.Value;
                                    break;
                                case PlayerPrefsKey.Level:
                                    Level = int.Parse(attribute.Value);
                                    break;
                                case PlayerPrefsKey.Mail:
                                    Mail = attribute.Value;
                                    break;
                                case PlayerPrefsKey.MailVerified:
                                    MailVerified = bool.Parse(attribute.Value);
                                    break;
                                case PlayerPrefsKey.NickName:
                                    NickName = attribute.Value;
                                    break;
                                case PlayerPrefsKey.Profile:
                                    Profile = attribute.Value;
                                    break;
                                case PlayerPrefsKey.WebSite:
                                    WebSite = attribute.Value;
                                    break;
                            }
                        }
                    }
                }
            }
            else message = "アカウント情報の取得に失敗しました。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (UserNotConfirmedException unce)
        {
            if (unce.Message == "User is not confirmed.") message = UserIsNotConfirmed;
            else message = unce.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string SetUserData(PlayerPrefsKey key, string value)
    {
        return SetUserData(new PlayerPrefsKey[]{key}, new string[]{value});
    }
    public string SetUserData(PlayerPrefsKey[] keys, string[] values)
    {
        string message = null;

        try
        {
            UpdateUserAttributesRequest attributesRequest = new UpdateUserAttributesRequest();
            attributesRequest.AccessToken = _token;

            for (int i = 0; i < keys.Length && i < values.Length; i++)
            {
                attributesRequest.UserAttributes.Add(new AttributeType {
                    Name = _attributeNameFromKey(keys[i]),
                    Value = values[i]
                });
            }

            UpdateUserAttributesResponse attributesResponse = _client.UpdateUserAttributes(attributesRequest);

            if (attributesResponse.HttpStatusCode != System.Net.HttpStatusCode.OK) message = "アカウント情報の上書きに失敗しました。";
            else message = GetUserData();
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (UserNotConfirmedException unce)
        {
            if (unce.Message == "User is not confirmed.") message = UserIsNotConfirmed;
            else message = unce.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }

    public string ChangePassword(string previous, string proposed)
    {
        string message = null;

        try
        {
            ChangePasswordRequest passwordRequest = new ChangePasswordRequest();
            passwordRequest.AccessToken = _token;
            passwordRequest.PreviousPassword = previous;
            passwordRequest.ProposedPassword = proposed;

            ChangePasswordResponse passwordResponse = _client.ChangePassword(passwordRequest);
            if (passwordResponse.HttpStatusCode == System.Net.HttpStatusCode.OK) Password = proposed;
            else message = "パスワードの変更に失敗しました。";
        }
        catch (NotAuthorizedException nae)
        {
            if (nae.Message == "Incorrect username or password.") message = IncorrectUsernameOrPassword;
            else if (nae.Message == "Access Token has expired") message = AccessTokenHasExpired;
            else if (nae.Message == "Access Token has been revoked") message = AccessTokenHasBeenRevoked;
            else if (nae.Message == "A client attempted to write unauthorized attribute") message = "不正な属性を書き込もうとしています。";
            else if (nae.Message == "User cannot be confirm. Current status is CONFIRMED") message = "既にAccess Codeで認証済みです。";
            else message = nae.Message;
        }
        catch (UserNotConfirmedException unce)
        {
            if (unce.Message == "User is not confirmed.") message = UserIsNotConfirmed;
            else message = unce.Message;
        }
        catch (Exception e)
        {
            message = e.Message;
        }

        return message;
    }
}